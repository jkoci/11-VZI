PROJNAME=mgraphsolver

CC=gcc
CXX=g++
RM=rm -f
CPPFLAGS=
LDFLAGS=
LDLIBS=

SRCS:=$(wildcard *.cpp)
OBJS:=$(subst .cpp,.o,$(SRCS))

all: $(PROJNAME)
	@echo "============ Running $(PROJNAME) ==========="
	@./$(PROJNAME) input.cnf
	
$(PROJNAME): $(OBJS) depend
	$(CXX) $(LDFLAGS) -o $(PROJNAME) $(OBJS) $(LDLIBS) 

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) *~ .depend
	$(RM) $(PROJNAME)

include .depend

.PHONY: clean distclean