#include "Multigraph.h"
MGraph MG;

/*     Node     */
Node::Node(int i, Node *p = NULL, Node *n = NULL) {
	id = i;
	prev = p;
	next = n;
	if(prev != NULL)
		prev->next = this;
	if(next != NULL)
		next->prev = this;
}

Node::Node(int i) {
	id = i;
	prev = NULL;
	next = NULL;
}

/*     MGraph     */
void MGraph::PrintNodes(void) {
	Node *n;
	n = Nhead;
	
	printf("Graph has following nodes\n");
	printf("=========================\n");
	while(n != NULL) {
		//printf("%d, ", n->id);
		PrintNode(n);
		n = n->next;
	}
	
	Task *t;
	t = Thead;
	
	printf("Following tasks are scheduled\n");
	printf("=============================\n");
	while(t != NULL) {
		printf("  [%d] --> [%d]\n", t->FromId, t->ToId);
		t = t->next;
	}
}

void MGraph::PrintNode(Node* N) {
	printf("[%d]\n", N->id);
	Edge *e;
	e = N->edgesOut;
	while(e != NULL) {
		printf("    Out -> [%d] (%d)\n",e->to->id, e->dist);
		e = e->nextOut;
	}
	e = N->edgesIn;
	while(e != NULL) {
		printf("    In  <= [%d] (%d)\n",e->from->id, e->dist);
		e = e->nextIn;
	}
	printf("\n");
}
Node* MGraph::getNodeById(int id) {
	Node* n = Nhead;
	
	while(n != NULL) {
		if(n->id == id)
			return n;
		n = n->next;
	}

	return NULL;
}

void MGraph::AddNode(Node* nd) {
	if(Nhead == NULL)
		Nhead = nd;
	else {
		Node* n;
		n = Nhead;
		while(n->next != NULL) {
			n = n->next;
		}
		n->next = nd;
		nd->prev = n;
	}
}
void MGraph::AddEdge(Node* from, Node* to, int distance) {
	Edge* NE = new Edge(from, to, distance);
	Edge* e;
	// link it into (f)->edgesOut
	e = from->edgesOut;
	if(from->edgesOut == NULL) {
		from->edgesOut = NE;
	} else {
		while(e->nextOut != NULL) {
			e = e->nextOut;
		}
		e->nextOut = NE;
	}
	// link it into (t)->edgesIn
	e = to->edgesIn;
	if(to->edgesIn == NULL) {
		to->edgesIn = NE;
	} else {
		while(e->nextIn != NULL) {
			e = e->nextIn;
		}
		e->nextIn = NE;
	}
}
void MGraph::AddTask(Task* T) {
	if(Thead == NULL) {
		Thead = T;
	} else {
		Task* list=Thead;
		while(list->next != NULL) {
			list = list->next;
		}
		list->next = T;
	}
}

void MGraph::Reset(void) {
	Node *n = Nhead;
	while(n != NULL) {
		n->distance = INT_MAX;
		n->IncomingNode = NULL;
		Qitm* Q = new Qitm();
		Q->node = n;
		if(Qhead == NULL) {
			Qhead = Q;
		} else {
			Qitm* list = Qhead;
			while(list->next != NULL) {
				list = list->next;
			}
			Q->prev = list;
			list->next = Q;
		}
		n = n->next;
	}
}

void MGraph::SortQ(void) {
	if(Qhead == NULL)
		return;
		
	int changed = 1;
	Qitm *A, *B;
	Node* tmp;
	while(changed) {
		changed = 0;
		tmp = NULL;
		A = Qhead;
		B = A->next;
		while(A->next != NULL) {
		   if(B == NULL)
		   	break;
			if(A->node->distance > B->node->distance) {
			   changed = 1;
			   tmp = A->node;
			   A->node = B->node;
			   B->node = tmp;
			}
		A = A->next;
		B = B->next;		
	   }
	}
}

void MGraph::PrintQ(void) {
	Qitm* Q = Qhead;
	while(Q != NULL) {
		//printf("[%d], [%d]\n", Q->node->distance, Q->node->id);
		Q = Q->next;
	}
}
void MGraph::Dijkstra(int FromId, int ToId) {
	printf("Task [%d] --> [%d]\n", FromId, ToId);
	
	Node* NodeFrom = getNodeById(FromId);
	Node* NodeTo   = getNodeById(ToId);
	Node* Neighbour;
	unsigned ndist;
	Qitm* N;
	Edge* E;
	
	Reset();
	NodeFrom->distance = 0;
	SortQ();
	
	while(Qhead != NULL) {
		N = Qhead;
		E = N->node->edgesOut;
		
	 	while(E != NULL) {
			Neighbour = E->to;
			ndist = N->node->distance + Distance(N->node, Neighbour);
			if(Neighbour->distance > ndist) {
				Neighbour->distance = ndist;
				Neighbour->IncomingNode = N->node;
			}
			E = E->nextOut; 
		}
		
		
		if(Qhead->prev != NULL) {
			delete Qhead->prev;	
		}
		Qhead = Qhead->next; 
   }
   delete Qhead;
   PrintPath(NodeTo);
//	DelQ();
}

void MGraph::PrintPath(Node* NodeTo) {
	Node* N = NodeTo;
	while(N->IncomingNode != NULL) {
		printf("[%d] <- ", N->id);
		N = N->IncomingNode;
	}
	printf("[%d = start]\n", N->id);
}

int MGraph::Distance(Node* A, Node* B) {
	Edge* E = A->edgesOut;
	int MD = INT_MAX;
	while(E != NULL) {
		if((E->dist < MD) && E->to == B)
			MD = E->dist;
		E = E->nextOut;
	}
	return MD;
}
/*     Edge     */
Edge::Edge(Node *f, Node *t, int d) {
	from = f;
	to = t;
	dist = d;
}

/*     Task     */
Task::Task(int f, int t) {
	FromId = f;
	ToId = t;
	next = NULL;
}