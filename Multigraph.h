#ifndef __MULTIGRAPH_H
#define __MULTIGRAPH_H

#include <stdio.h> // because NULL
#include <limits.h> // MAX_INT
struct MGraph;
struct Node;
struct Edge;
struct Task;
struct Qitm;

/* Definitions begin here */
struct MGraph {
	Node *Nhead;
	Task *Thead;
	Qitm *Qhead;
	void PrintNodes(void);
	void PrintNode(Node* N);
	Node* getNodeById(int id);
	void AddNode(Node* nd);
	void AddEdge(Node* from, Node* to, int dist);
	void AddTask(Task* T);
	
	void Reset(void);
	void Dijkstra(int FromId, int ToId);
	int Distance(Node* A, Node* B);
//	void DelQ(void);
	void SortQ(void);
	void PrintQ(void);
	void PrintPath(Node* NodeTo);
};

struct Node {
	Node *next;
	Node *prev;
	
	Edge *edgesIn;
	Edge *edgesOut;
	int id;

	unsigned distance;
	Node *IncomingNode;
	
	Node(int i, Node *p, Node *n);
	Node(int i);
};

struct Edge {
	Edge *nextIn;
	Edge *nextOut;
	
	Node *from;
	Node *to;
	
	int dist;
	
	Edge(Node* f, Node* t, int d);	
};

extern MGraph MG;

struct Task {
	int FromId;
	int ToId;
	Task* next;
	
	Task(int f, int t);
};

struct Qitm {
	Node *node;
	int  value;
	Qitm *next;
	Qitm *prev;
};
#endif