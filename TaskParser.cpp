#include "TaskParser.h"

void NodeDeclaration(char* line);
int match(char* needle, char* haystack);
void getEdgeParams(char* line, int *f, int *t, int *d);

TaskParser::TaskParser(char** task_name) {
	//printf("%s\n", *task_name);
	FILE* fr = fopen(*task_name, "r");
	if (fr == NULL) {
		has_error = E_FNOTOPENED;
		return;
	}
	
	char* line = NULL;
	size_t len = 0;
	TPParserState state;
	state = STATE_BEGIN;    
	int i = 0;
	
	// now for the FSM
	while (getline(&line, &len, fr) != EOF) {
		printf("Line %d: ",++i);
		// convert line to lower
		int j = 0;
		while(line[j] != '\0') {			
			line[j] = tolower(line[j]);
			if(line[j] == '\r' || line[j] == '\n')
				line[j] = '\0';
			j++;
		}

		if(line[0] == '\0') {
			printf("Empty line\n");
			continue;
		}
		
		if(line[0] == ';') {
			printf("Comment line\n");
			continue;
		}

		switch(state) {
			case STATE_BEGIN: 
				{
					char txt[10]="[nodes]";
					if(match(line, txt) != 0) {
						state = STATE_NODE_DECLARATION;
					} else {
						printf("Not matched [nodes]. That's an error.");	
						state = STATE_ERR;
					}
				}
				break;
				
			case STATE_NODE_DECLARATION:
				{
					char txt[10]="[edges]";
					if(match(line, txt) == 1) {
						state = STATE_EDGE_DECLARATION;
					} else {
						NodeDeclaration(line);	
					}
				} // end local scope
				break;
				
			case STATE_EDGE_DECLARATION:
				{ // scope start
					char txt[10]="[tasks]";
					if(match(line, txt) == 1) {
						state = STATE_TASK_DECLARATION;
					} else {
						printf("EDGE_DECLARATION '%s' ", line); 
						int f=0, t=0, d=0;
						getEdgeParams(line, &f, &t, &d);
						printf("Edge: [%d]-->[%d] (%d)", f, t, d);
						// Edge is parsed now
						Node *node_from, *node_to;
						// looking for node from
						node_from = MG.getNodeById(f);
						if(node_from == NULL) {
							fprintf(stderr, "Line '%d', requested node '%d' is not declared. That's an error.\n", i, f);
							state = STATE_ERR;
							break;
						}
						// looking for node to
						node_to = MG.getNodeById(t);
						if(node_to == NULL) {
							fprintf(stderr, "Line '%d', requested node '%d' is not declared. That's an error.\n", i, t);
							state = STATE_ERR;
							break;
						}
						// Construct edge
						MG.AddEdge(node_from, node_to, d); 
					}
				} // scope end    
				break;
			case STATE_TASK_DECLARATION:
			   { // scope start
			   	printf("TASK_DECLARATION '%s' ", line);
			   	int f=0, t=0, d=0;
					getEdgeParams(line, &f, &t, &d);
					
					printf("[%d] --> [%d]", f, t);
					Task* T = new Task(f, t);
					MG.AddTask(T);
				} // scope end
				break;	
			case STATE_ERR:
				has_error = E_PARSE;
				break;
				
			default:
				fprintf(stderr, "This should never happen: I'm in undefined state");
				has_error = E_PANIC;
				return;
				break;
		}	 // end case
		printf("\n");
		if(state == STATE_ERR)
			break;	
	}  // end while 
	       
	fclose(fr);
	if (line)
		free(line);
	if(state != STATE_TASK_DECLARATION) {
		has_error = E_PARSE;
	}
}

/*     helper functions     */
void NodeDeclaration(char* line) {
	int id = atoi(line);
	printf("NODE_DECLARATION '%s' Node: [%d]", line, id);
	Node *nd = new Node(id);
	// link in new node
	MG.AddNode(nd);
}

int match(char* needle, char* haystack) {
	int ismatch = 1, j = 0;
	while (1) {
		if(!needle[j] || !haystack[j])
			break;
		if(needle[j] != haystack[j]) {
			ismatch = 0;
		}
		j++;
	}
	
	if(ismatch == 1) {
	   printf("Matched %s", haystack);
	   return 1;
	} else {
		return 0;
	}
	
}

void getEdgeParams(char* line,int *f,int *t,int *d) {
	char *to, *di;
	int i, first=0;
	for(i = 0; line[i]; i++) {
		if(line[i] == ':') {
			if(first == 0) {
				to = &(line[i+1]);
				first = 1;
			} else {
				di = &(line[i+1]);
			}
		}
	}    
	*f = atoi(&(line[0]));
	if(to != NULL)
		*t = atoi(to);
	if(di != NULL)
		*d = atoi(di);
}