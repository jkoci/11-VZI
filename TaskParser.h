#ifndef __TASK_PARSER_H
#define __TASK_PARSER_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "Multigraph.h"

enum TPError {TP_OK, E_FNOTOPENED, E_PARSE, E_PANIC};

enum TPParserState {STATE_BEGIN, STATE_NODE_DECLARATION, STATE_EDGE_DECLARATION, STATE_TASK_DECLARATION, STATE_ERR};

struct TaskParser {
	TPError has_error = TP_OK;
//	char*[] parsed_task;
	
	TaskParser(char** task_name);	
};

#endif