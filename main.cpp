#include "main.h"

//extern MGraph MG;

int main(int argc, char* argv[]) {
	if(argc != 2) {
		fprintf(stderr, "This program should be run with single argument\n");
		fprintf(stderr, "%s [file with task]\n", argv[0]);
		return 1;
	}
	printf("This is mgraphsolver, running task '%s'.\n", argv[1]);
	
	TaskParser tp(&argv[1]);
	printf("Parser returned status\n>> %d <<\n", tp.has_error);
	if(tp.has_error) {
		return tp.has_error;
	}
	
	MG.PrintNodes();
	Task *T = MG.Thead;
	
	while(T != NULL) {
		printf("\n=================\n");
		MG.Dijkstra(T->FromId, T->ToId);
		T = T->next;
	}
	printf("\nThat's it!\n");
}